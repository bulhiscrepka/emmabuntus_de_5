#! /bin/bash

# lightdm_gtk_greeter_exec.sh --
#
#   This file permits to change theme on LightDM
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

. "$HOME/.config/user-dirs.dirs"

clear


file_lightdm=/etc/lightdm/lightdm-gtk-greeter.conf
file_lightdm_tmp=${file_lightdm}.tmp
name_dark_theme=Qogir-dark-largerborders
name_light_theme=Qogir-light-largerborders


enable_dark_theme=$1
name_dark_theme=$2
name_light_theme=$3

echo "sudo lightdm-gtk-greeter"


if [ ${enable_dark_theme} == "true" ] ; then
    name_theme=${name_dark_theme}
elif [ ${enable_dark_theme} == "false" ] ; then
    name_theme=${name_light_theme}
else
    exit 0
fi

size_name_theme=${#name_theme}

if test ${size_name_theme} -lt 20  ; then

    sed s/^theme-name[^$]*/theme-name=${name_theme}/ ${file_lightdm} | sudo tee ${file_lightdm_tmp}

    sudo cp ${file_lightdm_tmp} ${file_lightdm}

    sudo rm ${file_lightdm_tmp}

fi



