#! /bin/bash


# emmabuntus_choose_manager_setting.sh --
#
#   This file permits to choose manager settings beetween XFCE and LXQt desktop
#   for the Emmabuntüs Distrib
#
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


########################################################################################################



clear

if ps -A | grep "xfce4-session" ; then

    echo "xfce4-session"
    xfce4-settings-manager

elif ps -A | grep "lxsession" ; then

echo "lxde-session"

elif ps -A | grep "openbox" ; then

    echo "openbox-session"

elif ps -A | grep "lxqt-session"
then

    echo "lxqt-session"
    lxqt-config

fi

